--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:38:50 12/29/2016
-- Design Name:   
-- Module Name:   D:/Lesson/TERM7/FPGA/TA_SHARIFIAM/PS2-keyboard-vhdl/PS2FDTB.vhd
-- Project Name:  PS2-keyboard-vhdl
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: PS2FD
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY PS2FDTB IS
END PS2FDTB;
 
ARCHITECTURE behavior OF PS2FDTB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT PS2FD
    PORT(
         CLK : IN  std_logic;
         Reset : IN  std_logic;
         PS2C : IN  std_logic;
         PS2D : IN  std_logic;
         RX_EN : IN  std_logic;
         RX_DONE : OUT  std_logic;
         Dout : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal Reset : std_logic := '0';
   signal PS2C : std_logic := '0';
   signal PS2D : std_logic := '0';
   signal RX_EN : std_logic := '0';

 	--Outputs
   signal RX_DONE : std_logic;
   signal Dout : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
	constant PS2CKP : time := 100000 ns;
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: PS2FD PORT MAP (
          CLK => CLK,
          Reset => Reset,
          PS2C => PS2C,
          PS2D => PS2D,
          RX_EN => RX_EN,
          RX_DONE => RX_DONE,
          Dout => Dout
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
	process
	begin
		rx_en <= '1';
      wait for CLK_period*10;
		
		ps2c<='1';         
		wait for PS2CKP/2;
		ps2c<='0';
		wait for PS2CKP/2;
	end process;

   -- Stimulus process
   stim_proc: process
   begin		
   
		wait for PS2CKP/4;
		ps2d <= '0';
		wait for PS2CKP;
		ps2d <= '1';
		wait for PS2CKP;
		ps2d <= '1';
		wait for PS2CKP;
		ps2d <= '0';
		wait for PS2CKP;
		ps2d <= '1';
		wait for PS2CKP;
		ps2d <= '1';
		wait for PS2CKP;
		ps2d <= '0';
		wait for PS2CKP;
		ps2d <= '0';
		wait for PS2CKP;
		ps2d <= '0';
		wait for PS2CKP;
		ps2d <= '1';			--parity bit
		wait for PS2CKP;
		ps2d <= '1';
		wait for PS2CKP;
      wait;
   end process;

END;
