----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:40:09 12/29/2016 
-- Design Name: 
-- Module Name:    PS2FD - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PS2FD is
    Port ( CLK,Reset : in  STD_LOGIC;
           PS2C,PS2D : in  STD_LOGIC;
           RX_EN : in  STD_LOGIC;
           RX_DONE : out  STD_LOGIC;
           Dout : out  STD_LOGIC_VECTOR (7 downto 0));
end PS2FD;

architecture Behavioral of PS2FD is
type statetype is (idle,GF,load) ;
signal cur_state,next_state : statetype;
signal cur_filter,next_filter : std_logic_vector(7 downto 0);
signal cur_ps2c,next_ps2c : std_logic:= '0';
signal cur_Frame,next_Frame : std_logic_vector(10 downto 0):=(others=>'0');
signal cur_CNT,next_CNT : unsigned(3 downto 0) :=(others=>'0');
signal fall_edge,rd : std_logic :='0';
signal dataout : std_logic_vector(7 downto 0) :=(others => '0');
signal parity : std_logic:='0';
begin
	------------------------------------search for falling edge---------------------------------
	process(CLK)
	begin
		if rising_edge(CLK) then
			if reset = '1' then
				cur_filter	<= (others => '0');
			else
				cur_filter <= next_filter;
				cur_ps2c	  <= next_ps2c;
			end if;
		end if;
	end process;
	
	next_filter <= ps2c & cur_filter(7 downto 1);
	next_ps2c   <='1' when cur_filter = "11111111" else
					  '0' when cur_filter = "00000000" else
						cur_ps2c;
	fall_edge	<= cur_ps2c and (not(next_ps2c));
	--------------------------------------------------------------------------------------------
			
	process(CLK)
	begin
		if rising_edge(CLK) then
			if reset = '1' then
				cur_state <= idle;
			else
				cur_state <= next_state;
				cur_frame <= next_frame;
				cur_CNT	 <= next_CNT;
				if rd = '1' then 
					Dout <= dataout;			--it's for registering out put until next data
				end if;
			end if;
		end if;
	end process;
	
	process(cur_state,cur_frame,cur_CNT,rx_en,PS2D,fall_edge,parity)
	begin
		rd <= '0';
		next_state <= cur_state;
		next_frame <= cur_frame;
		next_CNT	  <=cur_CNT;
		case cur_state is
				when idle =>
					if(fall_edge = '1' and rx_en = '1')then
						if( PS2D = '0' ) then
							next_frame <= PS2D & cur_frame(10 downto 1);
							next_CNT <= "1001";
							next_state<=GF;
						end if;
					end if;
				when GF	 =>
					if fall_edge = '1' then
						next_frame <= PS2D & cur_frame(10 downto 1);
						if cur_CNT = 0 then
							next_state <= load;
						else
							next_CNT <= cur_CNT - 1;
						end if;
					end if;
				when load =>
					next_state	<= idle;
					if(cur_frame(10) = '1' and (parity = not(cur_frame(9))))then
						rd<='1';
					end if;
		end case;
	end process;
	RX_DONE <= rd;
	parity <= cur_frame(1) xor cur_frame(2) xor cur_frame(3) xor cur_frame(4) xor cur_frame(5) xor cur_frame(6) xor cur_frame(7) xor cur_frame(8);
	
	dataout <= cur_frame( 8 downto 1) ;
				  
end Behavioral;

